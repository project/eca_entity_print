<?php

namespace Drupal\eca_entity_print\Plugin\Action;

use Drupal\Core\Access\AccessManagerInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\eca\Plugin\Action\ConfigurableActionBase;
use Drupal\entity_print\Plugin\EntityPrintPluginManagerInterface;
use Drupal\entity_print\Plugin\ExportTypeManagerInterface;
use Drupal\entity_print\PrintBuilderInterface;
use Drupal\file\Entity\File;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Print file document from entity render.
 *
 * @Action(
 *   id = "eca_entity_print_print_file_from_entity",
 *   label = @Translation("Print file document from entity"),
 *   description = @Translation("Print file document from a chosen entity and saves it as a file entity."),
 *   type = "entity"
 * )
 */
class PrintFileFromEntity extends ConfigurableActionBase {

  /**
   * Access manager.
   *
   * @var \Drupal\Core\Access\AccessManagerInterface
   */
  protected AccessManagerInterface $accessManager;

  /**
   * The Print builder service.
   *
   * @var \Drupal\entity_print\PrintBuilderInterface
   */
  protected PrintBuilderInterface $printBuilder;

  /**
   * The Entity Print plugin manager.
   *
   * @var \Drupal\entity_print\Plugin\EntityPrintPluginManagerInterface
   */
  protected EntityPrintPluginManagerInterface $entityPrintPluginManager;

  /**
   * The export type manager.
   *
   * @var \Drupal\entity_print\Plugin\ExportTypeManagerInterface
   */
  protected ExportTypeManagerInterface $exportTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): static {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->accessManager = $container->get('access_manager');
    $instance->printBuilder = $container->get('entity_print.print_builder');
    $instance->entityPrintPluginManager = $container->get('plugin.manager.entity_print.print_engine');
    $instance->exportTypeManager = $container->get('plugin.manager.entity_print.export_type');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function access($object, ?AccountInterface $account = NULL, $return_as_object = FALSE): bool|AccessResultInterface {
    $result = AccessResult::forbidden();
    if ($object instanceof FieldableEntityInterface) {
      $route_params = [
        'export_type' => $this->configuration['export_type'],
        'entity_id' => $object->id(),
        'entity_type' => $object->getEntityTypeId(),
      ];
      $result = $this->accessManager->checkNamedRoute('entity_print.view', $route_params, $account, $return_as_object);
    }
    return $return_as_object ? $result : $result->isAllowed();
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function execute(mixed $entity = NULL): void {
    if (!($entity instanceof FieldableEntityInterface)) {
      return;
    }
    $this->doExecute($entity);
  }

  /**
   * Does the actual execution.
   *
   * @param mixed $object
   *   The object to print.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function doExecute(mixed $object): void {
    $print_engine = $this->entityPrintPluginManager->createSelectedInstance($this->getExportType());
    $file_name = $this->getFileName() . '.' . $print_engine->getExportType()->getFileExtension();

    $uri = $this->printBuilder->savePrintable([$object], $print_engine, 'private', $file_name);

    if ($uri) {
      /** @var \Drupal\file\Entity\File $file */
      $file = File::create([
        'filename' => $file_name,
        'uri' => $uri,
        'status' => 1,
        'uid' => 1,
      ]);
      $file->setPermanent();
      $file->save();

      $token_name = trim($this->configuration['token_name']);
      if ($token_name === '') {
        $token_name = 'eca-entity-output-filename';
      }
      $this->tokenService->addTokenData($token_name, $file);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [
      'token_name' => '',
      'export_type' => 'pdf',
      'file_name' => '',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form['token_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Name of token'),
      '#default_value' => $this->configuration['token_name'],
      '#description' => $this->t('Name of the token available after file document from view render is generated. Contains the file entity.'),
      '#weight' => -60,
      '#eca_token_reference' => TRUE,
    ];
    $form['file_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('The exported file name'),
      '#description' => $this->t('Provide a name for the generated file.'),
      '#default_value' => $this->configuration['file_name'],
      '#required' => TRUE,
      '#weight' => -20,
      '#eca_token_replacement' => TRUE,
    ];
    $form['export_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Export type'),
      '#options' => $this->exportTypeManager->getFormOptions(),
      '#required' => TRUE,
      '#default_value' => $this->configuration['export_type'],
      '#weight' => -10,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {
    $this->configuration['token_name'] = $form_state->getValue('token_name');
    $this->configuration['file_name'] = $form_state->getValue('file_name');
    $this->configuration['export_type'] = $form_state->getValue('export_type');
  }

  /**
   * Get the configured document export type.
   *
   * @return string
   *   The document export type.
   */
  protected function getExportType(): string {
    return trim((string) $this->tokenService->replaceClear($this->configuration['export_type']));
  }

  /**
   * Get the configured file name.
   *
   * @return string
   *   The file name.
   */
  protected function getFileName(): string {
    if (!empty($this->configuration['file_name'])) {
      return trim((string) $this->tokenService->replaceClear($this->configuration['file_name']));
    }
    return uniqid('eca.entity_print.output', TRUE);
  }

}
