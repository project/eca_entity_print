<?php

namespace Drupal\eca_entity_print\Plugin\Action;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\views\ViewExecutable;

/**
 * Print file document from view render.
 *
 * @Action(
 *   id = "eca_entity_print_print_file_from_view",
 *   label = @Translation("Print file document from views output"),
 *   description = @Translation("Print file document from a chosen view output and saves it as a file entity.")
 * )
 */
class PrintFileFromView extends PrintFileFromEntity {

  /**
   * The view.
   *
   * @var \Drupal\views\ViewExecutable
   */
  protected ViewExecutable $view;

  /**
   * {@inheritdoc}
   */
  public function access($object, ?AccountInterface $account = NULL, $return_as_object = FALSE): bool|AccessResultInterface {
    $result = AccessResult::forbidden();
    if ($this->getView()) {
      $display = $this->view->getDisplay();
      if ($display->access($account)) {
        $result = AccessResult::allowed();
      }
    }
    return $return_as_object ? $result : $result->isAllowed();
  }

  /**
   * {@inheritdoc}
   */
  public function execute(mixed $entity = NULL): void {
    if (!$this->getView()) {
      return;
    }
    $this->doExecute($this->view);
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [
      'view_id' => '',
      'display_id' => 'default',
      'arguments' => '',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $views = [];
    /** @var \Drupal\views\Entity\View[] $all_views */
    $all_views = $this->entityTypeManager->getStorage('view')->loadMultiple();
    foreach ($all_views as $view) {
      if ($view->status()) {
        $views[$view->id()] = $view->label();
      }
    }
    $form['view_id'] = [
      '#type' => 'select',
      '#title' => $this->t('View'),
      '#default_value' => $this->configuration['view_id'],
      '#description' => $this->t('Select the view from the list. The view  will always return a list of complete entities.'),
      '#weight' => -50,
      '#options' => $views,
      '#required' => TRUE,
    ];
    $form['display_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Display'),
      '#default_value' => $this->configuration['display_id'],
      '#description' => $this->t('Write the view <code>display id</code> to execute. Set as default to use the default view configuration.'),
      '#weight' => -40,
    ];
    $form['arguments'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Arguments'),
      '#default_value' => $this->configuration['arguments'],
      '#description' => $this->t('Provide the Contextual filters of the view in order and one by line. This property supports tokens.'),
      '#weight' => -30,
    ];
    return parent::buildConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {
    $this->configuration['view_id'] = $form_state->getValue('view_id');
    $this->configuration['display_id'] = $form_state->getValue('display_id');
    $this->configuration['arguments'] = $form_state->getValue('arguments');
    parent::submitConfigurationForm($form, $form_state);
  }

  /**
   * Prepare and return view with optional arguments applied.
   *
   * The view gets loaded, its executable set to $this->view and the given
   * display being prepared with optional arguments.
   *
   * @return bool
   *   TRUE, if the view got loaded successfully, FALSE otherwise.
   */
  protected function getView(): bool {
    $view_id = $this->getViewId();
    if ($view_id === '') {
      return FALSE;
    }

    /** @var \Drupal\views\Entity\View|null $view */
    $view = $this->entityTypeManager->getStorage('view')->load($view_id);
    if ($view === NULL || !$view->status()) {
      return FALSE;
    }
    $this->view = $view->getExecutable();
    $display_id = $this->getDisplayId();
    if ($display_id !== '') {
      if (!$this->view->setDisplay($display_id)) {
        return FALSE;
      }
    }
    else {
      $this->view->initDisplay();
    }
    $args = [];
    foreach (explode('/', $this->getArguments()) as $argument) {
      if ($argument !== '') {
        $args[] = $argument;
      }
    }
    $this->view->setArguments($args);
    return TRUE;
  }

  /**
   * Get the configured view ID.
   *
   * @return string
   *   The view ID.
   */
  protected function getViewId(): string {
    return trim((string) $this->tokenService->replaceClear($this->configuration['view_id']));
  }

  /**
   * Get the configured display ID.
   *
   * @return string
   *   The display ID.
   */
  protected function getDisplayId(): string {
    return trim((string) $this->tokenService->replaceClear($this->configuration['display_id']));
  }

  /**
   * Get the configured Views arguments.
   *
   * @return string
   *   The arguments, multiple arguments are separated by "/".
   */
  protected function getArguments(): string {
    return trim((string) $this->tokenService->replaceClear($this->configuration['arguments']));
  }

}
